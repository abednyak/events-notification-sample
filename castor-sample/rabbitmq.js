const q = 'project-update-events';
const config = require('./config');
const open = require('amqplib').connect(`amqp://${config.rabbitmq.host}:${config.rabbitmq.port}`);

async function rbmqCreatePublisher() {
    return open.then(function(conn) {
        return conn.createChannel();
    }).then(function(ch) {
        console.log(ch);
        return ch.assertQueue(q).then(function(ok) {
            console.log(`Asserted queue: ${ok.queue}`)
            return ch;
        });
    }).catch((err) => {
        throw new Error(err.message);
    });
}

module.exports = rbmqCreatePublisher;