module.exports = {
    appPort: process.env.APPLICATION_PORT || 3002,
    rabbitmq: {
        host: process.env.RBMQ_HOST || 'localhost',
        port: process.env.RBMQ_PORT || 5672
    },
};