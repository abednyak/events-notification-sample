const {body, validationResult} = require("express-validator");

const validateTriggerBody = () => {
  return [
    body('clientId', "Must be number and exist!").toInt().isInt().exists(),
    body('projectId', "Must be number and exist!").toInt().isInt().exists(),
    body('event', "Must be string and exist!").isString().exists()
  ]
}

const validate = (req, res, next) => {
  const errors = validationResult(req);

  if (errors.isEmpty()) {
    return next()
  }

  if (!errors.isEmpty()) {
    console.log(errors.array())
    return res.status(400).json({errors: errors.array()});
  }
}

module.exports = {validateTriggerBody, validate}