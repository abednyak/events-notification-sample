const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const ch = require('./rabbitmq');
const {validateTriggerBody, validate} = require("./validator_middleware");
const q = 'project-update-events';

app.use(bodyParser.json())

app.get('/echo', (req, res) => {
    res.status(200).json({ alive: true });
})

app.put('/trigger', validateTriggerBody(), validate, async (req, res) => {
    try {
        const data = req.body;
        (await ch()).sendToQueue(q, Buffer.from(JSON.stringify(data)));
        res.sendStatus(204);
    } catch (err) {
        console.log('ERROR:', err);
        res.status(500).json(err.message);
    }

})

module.exports = app;