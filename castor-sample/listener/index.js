const express = require("express")
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express()

app.use(cors())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// Route
app.post('/', (req, res) => {
  console.log(req.body)
  res.status(200).json({message: "OK"})
})

app.listen(7000, () => {
  console.log("Server is up!")
})