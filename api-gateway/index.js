const config = require('./config');
const app = require('./app');
const port = config.appPort;

app.listen(port, async () => {
    console.log(`Example app listening at http://localhost:${port}`)
})