const axios = require('axios');
const utils = require('../utils');
const config = require('../config');
const url = utils.urlLinkGenerator(
    config.notificationManagerScheme,
    config.notificationManagerHost,
    config.notificationManagerPort
);

async function proxySubscribeEventRequest(data) {
    console.log(`CURL: ${url}/save`);
    return await axios.post(`${url}/save`, data)
        .then(function (response) {
            return response;

        })
        .catch(function (error) {
            console.log('Error:' + error);
            throw new Error(error.message);
        });
}

module.exports = {
    proxySubscribeEventRequest
};