function urlLinkGenerator(schema, host, port) {
    return `${schema}://${host}:${port}`
}

module.exports = {
    urlLinkGenerator
}