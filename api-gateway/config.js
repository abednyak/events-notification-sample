module.exports = {
    appPort: process.env.APPLICATION_PORT || 3001,
    notificationManagerScheme: process.env.NOTIF_MNGR_SCHEME || 'http',
    notificationManagerHost: process.env.NOTIF_MNGR_HOST || 'localhost',
    notificationManagerPort: process.env.NOTIF_MNGR_PORT || 3000
};