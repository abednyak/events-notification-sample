const express = require('express')
const bodyParser = require('body-parser')
const proxyService = require('./services/proxy-service')
const {validateTriggerBody, validate} = require("./validator_middleware");

const app = express()

app.use(bodyParser.json())

app.get('/echo', (req, res) => {
    res.status(200).json({ alive: true });
})

app.put('/api/event', validateTriggerBody(), validate, async (req, res) => {
    try {
        const data = {
            clientId: req.body.clientId,
            companyUrl: req.body.companyUrl
        };
        const response = await proxyService.proxySubscribeEventRequest(data);
        if (!response) {
            res.status(403).error({
                TYPE: 'API_EVENT',
                DESCRIPTION: 'API_EVENT was not proxied'
            })
        }
        res.sendStatus(204);
    } catch (err) {
        console.log('ERROR:', err);
        res.status(500).json(err.message);
    }
})

module.exports = app;