const longPollingService = require('../services/longpolling-service')

test('Should return NULL by default', () => {
    const expectedResult = null;
    expect(longPollingService.getLongPollingData()).toEqual(expectedResult);
});