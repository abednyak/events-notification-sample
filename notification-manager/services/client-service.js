const NotificationSubscriber = require('../database/models/NotificationSubscriber')

async function saveNotificationSubscriber(data) {
    try {
        const subscriber = await NotificationSubscriber.create({
            clientId: data.clientId,
            companyUrl: data.companyUrl
        });
        return subscriber;
    } catch(err) {
        throw new Error(err.message);
    }
}

async function findNotificationSubscriberByClientId(data) {
    try {
        const subscriber = await NotificationSubscriber.findOne({
            where: {
                clientId: data.clientId
            },
        }).then((sub) => {
            return sub;
        });
        return subscriber;

    } catch(err) {
        throw new Error(err);
    }
}

module.exports = {
    saveNotificationSubscriber,
    findNotificationSubscriberByClientId
}