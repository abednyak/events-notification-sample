const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const longpoll = require("express-longpoll")(app);
const rbmq = require("./rabbitmq");
const clientService = require('./services/client-service');

longpoll.create("/xerox/poll");

app.use(bodyParser.json());

app.get('/echo', (req, res) => {
    res.status(200).json({ alive: true });
});

app.post('/save', async (req, res) => {
    try {
        const data = req.body;
        await clientService.saveNotificationSubscriber(data);
        res.sendStatus(204);
    } catch (err) {
        console.log('ERROR:', err);
        res.status(500).json(err.message);
    }
});

setInterval(async () => {
    let data = "I'm alive"
    const lpData = rbmq.getLongPollingData();
    if (lpData) {
        const sub = await clientService.findNotificationSubscriberByClientId(lpData)
        if (sub) data = lpData
    }
    console.log(`INFO: '${data}' was published to /xerox/poll`);
    longpoll.publish("/xerox/poll", data);
}, 60000);

module.exports = app;