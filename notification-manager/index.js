const config = require('./config');
const app = require('./app');
const port = config.appPort;
const sequelize = require('./database/index')
const rbmqConsumer = require('./rabbitmq').rbmqCreateConsumer;

app.listen(port, async () => {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
    await rbmqConsumer();
    console.log(`Example app listening at http://localhost:${port}`)
})