module.exports = {
    appPort: process.env.APPLICATION_PORT || 3000,
    rabbitmq: {
        host: process.env.RBMQ_HOST || 'localhost',
        port: process.env.RBMQ_PORT || 5673
    },
    database: {
        username: process.env.DB_USERNAME || `user`,
        password: process.env.DB_PASSWORD || `password`,
        name: process.env.DB_NAME || `sample`,
        host: process.env.DB_HOST || `localhost`,
        port: process.env.DB_PORT || 3307,
        dialect: `mysql`,
    }

};