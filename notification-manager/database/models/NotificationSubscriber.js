const { DataTypes } = require('sequelize');
const sequelize = require('../index');

const NotificationSubscriber = sequelize.define('NotificationSubscriber', {
    id: {
        type: DataTypes.UUID,
        autoIncrement: true,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
    },
    clientId: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    companyUrl: {
        type: DataTypes.STRING,
        allowNull: false
    }
},{
    freezeTableName: true
});

module.exports = NotificationSubscriber;