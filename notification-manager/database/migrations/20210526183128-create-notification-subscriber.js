'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      await queryInterface.createTable('NotificationSubscriber', {
          id: {
              type: Sequelize.UUID,
              allowNull: false,
              primaryKey: true,
              defaultValue: Sequelize.UUIDV4,
          },
          clientId: {
              type: Sequelize.INTEGER,
              allowNull: false,
          },
          companyUrl: {
              type: Sequelize.STRING,
              allowNull: false,
          },
          createdAt: {
              allowNull: false,
              type: Sequelize.DATE,
          },
          updatedAt: {
              allowNull: false,
              type: Sequelize.DATE,
          },
      });
  },

  down: async (queryInterface, Sequelize) => {
      await queryInterface.dropTable('NotificationSubscriber');
  }
};
