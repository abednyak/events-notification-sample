const config = require('./config');
const q = 'tasks';
const open = require('amqplib').connect(`amqp://${config.rabbitmq.host}:${config.rabbitmq.port}`);
let data = null;

async function rbmqCreateConsumer() {
    return open.then(function(conn) {
        return conn.createChannel();
    }).then(function(ch) {
        return ch.assertQueue(q).then(async function(ok) {
            return ch.consume(q, function(msg) {
                if (msg !== null) {
                    const msgContent = JSON.parse(msg.content.toString())
                    console.log(`INFO: queue '${q}' --- consume ${JSON.stringify(msgContent)}`);
                    data = JSON.parse(msg.content.toString());
                    ch.ack(msg);
                }
            });
        });
    }).catch((err) => {
        throw new Error(err);
    })};

function getLongPollingData() {
    if (data) {
        let x;
        x = data;
        data = null;
        return x
    }
    return data;
}

module.exports = {
    rbmqCreateConsumer,
    getLongPollingData,
};