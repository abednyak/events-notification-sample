module.exports = {
    username: process.env.DB_USERNAME || `user`,
    password: process.env.DB_PASSWORD || `password`,
    database: process.env.DB_NAME || `sample`,
    host: process.env.DB_HOST || `localhost`,
    port: process.env.DB_PORT || 3307,
    dialect: `mysql`,
    seederStorage: `sequelize`,
    seederStorageTableName: `SequelizeSeederMeta`
};
